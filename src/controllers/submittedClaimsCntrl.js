angular.module('claimSpiff',[]);

export default class SpiffClaimCntrl{
    constructor(
                $scope,
                $location
                ) {
        var claimed_records=window.localStorage.getItem("submittedClaims");
        console.log("claimed_records",JSON.parse(claimed_records));
        $scope.claimSpiffs=JSON.parse(claimed_records);
        $scope.finish_submit=function(){
            //window.localStorage.clear();
            $location.path("/");
        }
        //get the Loccal date and time from UTC
        function convertUTCDateToLocalDate(date) {
            var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
            var offset = date.getTimezoneOffset() / 60;
            var hours = date.getHours();
            newDate.setHours(hours - offset);
            return newDate;
        }
        var date = convertUTCDateToLocalDate(new Date($scope.claimSpiffs[0].spiffClaimedDate));
        $scope.claimSpiffs[0].spiffClaimedDate=date.toLocaleString();

       /* var svrDate = new Date($scope.claimSpiffs[0].spiffClaimedDate);
        var tzo = ((new Date()).getTimezoneOffset() / 60) * (-1);
        var userTime = new Date(svrDate.setHours(svrDate.getHours() + tzo)).toLocaleString();
        console.log("userTime",userTime);*/
    }
}

SpiffClaimCntrl.$inject = [
    '$scope',
    '$location'
];
