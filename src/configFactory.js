import Config from './config';
import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SpiffApiGatewayServiceSdkConfig} from 'spiff-api-gateway-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {SessionManagerConfig} from 'session-manager';

export default class ConfigFactory {

    /**
     * @param {object} data
     * @returns {Config}
     */
    static construct(data):Config {

        const identityServiceSdkConfig =
            new IdentityServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const spiffApiGatewayServiceSdkConfig =
            new SpiffApiGatewayServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );
        const claimSpiffServiceSdkConfig =
            new ClaimSpiffServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const sessionManagerConfig =
            new SessionManagerConfig(
                data.precorConnectApiBaseUrl,
                data.sessionManagerConfig.loginUrl,
                data.sessionManagerConfig.logoutUrl
            );

        return new Config(
            identityServiceSdkConfig,
            spiffApiGatewayServiceSdkConfig,
            claimSpiffServiceSdkConfig,
            sessionManagerConfig
        );

    }

}
